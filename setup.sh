#!/bin/bash
############################################################
#                                                          #
#       LAZY ARCH SETUP SCRIPT                             #
#       NO DESKTOP ENVIRONMENT, ONLY BASIC UTILITIES       #
#                                                          #
############################################################

## PART 1
## Enabling parallel downloads for pacman
printf '\033c'
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 5/" /etc/pacman.conf
pacman --noconfirm -Sy dialog archlinux-keyring
dialog --defaultno --title "Welcome to Pixel installer for Arch linux" --yesno "THIS SCRIPT WILL COMPLETELY WIPE YOUR /dev/nvme0n1\n\nUSE IT ONLY IF YOU WANNA WIPE YOUR ENTIRE DRIVE" 15 60 || exit

# Creating the partitions with fdisk, no user interaction required
# Formatting to ext4 by default, refer to Artix-openrc-btrfs for btrfs options
cat <<EOF | fdisk /dev/nvme0n1
d

d

d
g
n


+512M
t
1

n


+120G
n



w
EOF

  yes | mkfs.ext4 -L ROOT /dev/nvme0n1p2
  yes | mkfs.ext4 -L HOME /dev/nvme0n1p3
  mkfs.vfat -F 32 /dev/nvme0n1p1
  fatlabel /dev/nvme0n1p1 BOOT

mount /dev/nvme0n1p2 /mnt
mkdir -p /mnt/boot/efi
mkdir -p /mnt/home
mount /dev/nvme0n1p3 /mnt/home
mount /dev/nvme0n1p1 /mnt/boot/efi

# Installing the base system

pacstrap /mnt base base-devel linux linux-firmware intel-ucode vim
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^##PART 2$/d' setup.sh > /mnt/setup2.sh
chmod +x /mnt/setup2.sh
arch-chroot /mnt ./setup2.sh
exit

##PART 2

# Enabling parallel downloads for pacman
printf '\033c'
pacman --noconfirm --needed -Sy archlinux-keyring dialog libnewt zsh
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 5/" /etc/pacman.conf

# Setting up hardware clock with my time zone
ln -sf /usr/share/zoneinfo/Africa/Algiers /etc/localtime
hwclock --systohc

# Creating a 4GB swap file
dd if=/dev/zero of=/swapfile bs=2G count=2 status=progress
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
printf "\n\n/swapfile    none    swap  defaults    0 0\n" >> /etc/fstab

# Generating the Locales
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf

# Getting the Hostname, user interaction required
dialog --no-cancel --inputbox "Enter your computer hostname." 10 60 2> comp
echo $(cat comp) > /etc/hostname
printf '\033c'
printf "\n127.0.0.1       localhost\n::1             localhost\n127.0.1.1       $(cat comp).localdomain $(cat comp)\n">> /etc/hosts
rm comp

# Username and password prompt:
	name=$(whiptail --inputbox "Please enter a name for the user account." 10 60 3>&1 1>&2 2>&3 3>&1) || exit 1
		while ! echo "$name" | grep -q "^[a-z_][a-z0-9_-]*$"; do
			name=$(whiptail --nocancel --inputbox "Username not valid. Give a username beginning with a letter, with only lowercase letters, - or _." 10 60 3>&1 1>&2 2>&3 3>&1)
		done
		pass1=$(whiptail --nocancel --passwordbox "Enter a password for the user account." 10 60 3>&1 1>&2 2>&3 3>&1)
		pass2=$(whiptail --nocancel --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1)
		while ! [ "$pass1" = "$pass2" ]; do
			unset pass2
			pass1=$(whiptail --nocancel --passwordbox "Passwords do not match.\\n\\nEnter password again." 10 60 3>&1 1>&2 2>&3 3>&1)
			pass2=$(whiptail --nocancel --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1)
		done

# Creating the user account and adding it to the wheel group
useradd -mG wheel -s /bin/zsh $name
echo "$name:$pass1" | chpasswd
unset pass1

# Setting up GRUB Bootloader
pacman --noconfirm --needed -S grub efibootmgr
grub-install
#sed -i 's/quiet/pci=noaer/g' /etc/default/grub
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg


# Installing basic and needed packages for my workflow
pacman -S --noconfirm xorg-server xorg-xinit xorg-xkill xorg-xsetroot xorg-xbacklight xorg-xprop \
     noto-fonts noto-fonts-emoji noto-fonts-cjk ttf-joypixels ttf-font-awesome \
     mpv zathura zathura-pdf-mupdf ffmpeg imagemagick  \
     fzf man-db xwallpaper unclutter xclip maim \
     zip unzip unrar p7zip xdotool brightnessctl  \
     dosfstools ntfs-3g git pipewire pipewire-pulse \
     vim neovim arc-gtk-theme rsync firefox dash \
     xcompmgr libnotify dunst slock jq valgrind clang llvm bind rust go \
     dhcpcd networkmanager network-manager-applet rsync pamixer openssh

# Enabling Network manager for internet connection
systemctl enable NetworkManager
systemctl start NetworkManager

# Making dash the default shell in /bin/sh for faster shell script execution
rm /bin/sh
ln -s dash /bin/sh

# Enabling wheel group to use sudo
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# Sending the rest of the script to the home directory of newly created user
sc3_path=/home/$name/setup3.sh
sed '1,/^## PART 3$/d' setup2.sh > $sc3_path
chown $name:$name $sc3_path
chmod +x $sc3_path
su -c $sc3_path -s /bin/sh $name
exit

## PART 3

printf '\033c'
cd $HOME

# Installing paru, an aur helper
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si
dialog --title "Finally you reached the end!" --yesno "Install script finished\n\nNow run umount -R /mnt and reboot"  10 30
exit
